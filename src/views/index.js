import React, { useEffect, useState } from "react";
import Timer from "views/Timer";

const sound = new Audio("./sounds/sound.mp3");

const Main = () => {
  const [blindList] = useState([
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
  ]);
  const [index, setIndex] = useState(0);
  const [timerRef, setTimerRef] = useState(null);
  const [running, setRunning] = useState(false);
  const [inputTime, setInputTime] = useState({
    hours: 0,
    minutes: 0,
    seconds: 0,
  });
  const [time, setTime] = useState({ hours: 0, minutes: 0, seconds: 0 });

  const onTimerEnd = () => {
    sound.play();
    if (index < blindList.length - 1) {
      setTime(inputTime);
      setIndex((prev) => prev + 1);
    }
  };

  useEffect(() => {
    if (index > 0 && timerRef) timerRef.start();
  }, [index]);

  useEffect(() => {
    setTime({ ...inputTime });
  }, [inputTime]);

  const handleChange = (e) => {
    if (e.target.value < 0) return;
    setInputTime((prev) => ({
      ...prev,
      [e.target.name]: Number(e.target.value),
    }));
  };

  const getRemainingTime = () => {
    let total = 0;
    total += time.seconds;
    total += time.minutes * 60;
    total += time.hours * 60 * 60;
    return total;
  };

  const onTimerStop = (hrs, mins, secs) => {
    setTime({ hours: hrs, minutes: mins, seconds: secs });
    setRunning(false);
  };

  const reset = () => {
    setIndex(0);
    setTime({ ...inputTime });
  };

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100%",
        flexDirection: "column",
      }}
    >
      <div style={{ display: "flex", flexDirection: "column", width: 250 }}>
        <div
          style={{ flex: 1, justifyContent: "space-between", display: "flex" }}
        >
          <label htmlFor="hoursInput">Horas</label>
          <input
            id="hoursInput"
            placeholder="Horas"
            value={inputTime.hours}
            name="hours"
            onChange={handleChange}
            type="number"
            disabled={running}
          />
        </div>
        <div
          style={{ flex: 1, justifyContent: "space-between", display: "flex" }}
        >
          <label htmlFor="minutesInput">Minutos</label>
          <input
            id="minutesInput"
            placeholder="Minutos"
            value={inputTime.minutes}
            name="minutes"
            onChange={handleChange}
            type="number"
            disabled={running}
          />
        </div>
        <div
          style={{ flex: 1, justifyContent: "space-between", display: "flex" }}
        >
          <label htmlFor="secondsInput">Segundos</label>
          <input
            id="secondsInput"
            placeholder="Segundos"
            value={inputTime.seconds}
            name="seconds"
            onChange={handleChange}
            type="number"
            disabled={running}
          />
        </div>
      </div>
      <span
        style={{ fontSize: 50, fontWeight: "bold" }}
      >{`Jogo Atual: ${blindList[index]}`}</span>
      <Timer
        style={{
          fontSize: 100,
          backgroundColor: "black",
          color: "white",
          padding: 10,
          borderRadius: 12,
          marginBottom: 12,
          marginTop: 12,
        }}
        ref={setTimerRef}
        time={getRemainingTime()}
        onTimerEnd={onTimerEnd}
        onTimerStart={() => setRunning(true)}
        onTimerStop={onTimerStop}
      />
      {timerRef && (
        <div>
          <button disabled={running} onClick={timerRef.start}>
            Start
          </button>
          <button disabled={!running} onClick={timerRef.stop}>
            Stop
          </button>
          <button disabled={running} onClick={reset}>
            Reset
          </button>
        </div>
      )}
    </div>
  );
};

export default Main;
