import React from "react";

class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.hrs = 0;
    this.mins = 0;
    this.secs = 0;
    this.timer = null;
    this.lastTime = props.time;
    this.state = {};
  }

  init = () => {
    const initialization = this.parseTime(this.lastTime);
    this.hrs = initialization.hours;
    this.mins = initialization.minutes;
    this.secs = initialization.seconds;
    this.setState({});
  };

  getRemainingTime = () => {
    let total = 0;
    total += this.secs;
    total += this.mins * 60;
    total += this.hrs * 60 * 60;
    return total;
  };

  runTimer = () => {
    this.timer = setInterval(() => {
      if (this.secs > 0) {
        this.secs = this.secs - 1;
        this.setState({});
      } else {
        if (this.mins > 0) {
          this.secs = 59;
          this.mins = this.mins - 1;
        } else {
          if (this.hrs > 0) {
            this.mins = 59;
            this.hrs = this.hrs - 1;
          } else {
            clearInterval(this.timer);
            this.timer = null;
            if (this.props.onTimerEnd) this.props.onTimerEnd();
          }
        }
        this.setState({});
      }
    }, 1000);
  };

  componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    if (prevProps != this.props) {
      this.lastTime = this.props.time;
      this.init();
      return true;
    }
  }

  stop = () => {
    this.lastTime = this.getRemainingTime();
    clearInterval(this.timer);
    this.timer = null;
    this.init();
    if (this.props.onTimerStop)
      this.props.onTimerStop(this.hrs, this.mins, this.secs);
  };

  parseTime = (inSecs) => {
    let seconds = 0;
    let minutes = 0;
    let hours = 0;

    seconds = inSecs % 60;
    minutes = parseInt(inSecs / 60);
    hours = parseInt(minutes / 60);
    minutes = minutes % 60;

    return { seconds, minutes, hours };
  };

  start = () => {
    if (this.timer) return;
    if (this.props.onTimerStart) this.props.onTimerStart();
    const { seconds, minutes, hours } = this.parseTime(this.lastTime);
    this.secs = seconds;
    this.mins = minutes;
    this.hrs = hours;
    this.setState({});
    this.runTimer();
  };

  render() {
    return (
      <div style={this.props.style}>
        {`${this.hrs > 9 ? this.hrs : `0${this.hrs}`}:${
          this.mins > 9 ? this.mins : `0${this.mins}`
        }:${this.secs > 9 ? this.secs : `0${this.secs}`}`}
      </div>
    );
  }
}

export default Timer;
